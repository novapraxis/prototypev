class DailyReport < BillDetail

		def self.bill_detail_summary_by_date(date, store, pos_desc)
				BillDetail.select(:product_id,
                          :product_description,
                          "SUM(billdetail_amount) as billdetail_amount",
                          "SUM(billdetail_net1) as billdetail_net1",
                          "SUM(billdetail_net2) as billdetail_net2",
                          "SUM(billdetail_net3) as billdetail_net3",
                          "SUM(billdetail_net4) as billdetail_net4").where(
                          "billdetail_date = ? AND store_id = ?  AND pos_desc = ?",
                          "#{date}", "#{store}", "#{pos_desc}").group(
                          :product_id, :product_description)
    end

end
