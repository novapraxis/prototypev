class TurnPaymethod < ApplicationRecord
  def self.turn_summary_by_date(date, store, pos_desc)
    
    TurnPaymethod.select(:turn_id).where("date = ? AND store_id = ? AND pos_desc = ?", "#{date}", "#{store}", "#{pos_desc}").pluck(:turn_id)

  end

  def self.daily_summary_by_date(store, turn)
    TurnPaymethod.select(:paymethod_id, :paymethod_description, :system_total, :cashbox_total, :difference).where(:turn_id => "#{turn}")
  end
end
