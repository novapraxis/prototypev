class ReportsController < ApplicationController
	before_action :authenticate_user!

	def index
  end

	def daily
    if params[:date]
		@products = BillDetail.bill_detail_summary_by_date(params[:date], params[:store], params[:pos_desc])
    @sum = {
      :net1 => @products.map {|s| s['billdetail_net1']}.reduce(0, :+),
      :net2 => @products.map {|s| s['billdetail_net2']}.reduce(0, :+),
      :net3 => @products.map {|s| s['billdetail_net3']}.reduce(0, :+),
      :net4 => @products.map {|s| s['billdetail_net4']}.reduce(0, :+),
      :amount => @products.map {|s| s['billdetail_amount']}.reduce(0, :+)
    }

    @turns = TurnPaymethod.turn_summary_by_date(params[:date], params[:store], params[:pos_desc])
    @turnpaymethod = TurnPaymethod.daily_summary_by_date(params[:store], @turns.first)
    @sumpay = {
      :system_total => @turnpaymethod.map {|s| s['system_total']}.reduce(0, :+),
      :cashbox_total => @turnpaymethod.map {|s| s['cashbox_total']}.reduce(0, :+),
      :difference => @turnpaymethod.map {|s| s['difference']}.reduce(0, :+),
    }
    end

    @stores = BillDetail.uniq.pluck(:store_id)
    @pos = BillDetail.uniq.pluck(:pos_desc)
	end

  def show
  end
end
