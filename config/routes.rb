Rails.application.routes.draw do
  root 'menu#index'

  get 'reports/index'
	get	'reports/daily'
  devise_for :users

  resource :reports do
    get 'daily'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
