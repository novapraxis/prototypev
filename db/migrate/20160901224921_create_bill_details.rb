class CreateBillDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :bill_details do |t|
      t.integer :bill_number
      t.decimal :product_id
      t.text :product_description
      t.float :product_price
      t.integer :billdetail_amount
      t.float :billdetail_total
      t.float :billdetail_net1
      t.float :billdetail_net2
      t.float :billdetail_net3
      t.float :billdetail_net4
      t.float :billdetail_discount
      t.float :billdetail_tax
      t.float :billdetail_tip
      t.string :store_id
      t.string :pos_id

      t.timestamps
    end
  end
end
