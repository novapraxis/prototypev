class CreateTurnPaymethods < ActiveRecord::Migration[5.0]
  def change
    create_table :turn_paymethods do |t|
      t.string :paymethod_id
      t.string :paymethod_description
      t.decimal :system_total
      t.decimal :cashbox_total
      t.decimal :difference
      t.string :turn_id
      t.string :store_id
      t.string :pos_id
      t.timestamp :creation_time

      t.timestamps
    end
  end
end
