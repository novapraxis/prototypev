class CreateBills < ActiveRecord::Migration[5.0]
  def change
    create_table :bills do |t|
      t.integer :bill_number
      t.datetime :bill_datetime
      t.float :bill_tax
      t.float :bill_tip
      t.float :bill_total
			t.float :bill_discount
      t.string :store_id
      t.string :pos_id

      t.timestamps
    end
  end
end
