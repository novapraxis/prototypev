class AddDateColToTurnPaymethods < ActiveRecord::Migration[5.0]
  def change
    add_column :turn_paymethods, :date, :date
  end
end
