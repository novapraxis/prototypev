class AddPosDescColToTurnPaymethods < ActiveRecord::Migration[5.0]
  def change
    add_column :turn_paymethods, :pos_desc, :string
  end
end
