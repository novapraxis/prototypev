class AddPosDescToBillDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :bill_details, :pos_desc, :string
  end
end
