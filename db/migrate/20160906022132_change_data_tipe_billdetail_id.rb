class ChangeDataTipeBilldetailId < ActiveRecord::Migration[5.0]
  def change
    change_column(:bill_details, :product_id, :integer)
  end
end
