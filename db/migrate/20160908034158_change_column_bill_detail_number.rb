class ChangeColumnBillDetailNumber < ActiveRecord::Migration[5.0]
  def change
    change_column :bill_details, :bill_number, :bigint
  end
end
