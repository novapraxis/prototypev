class AddBilldetailDateToBillDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :bill_details, :billdetail_date, :date
  end
end
