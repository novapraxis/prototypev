# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160922003630) do

  create_table "bill_details", force: :cascade do |t|
    t.bigint   "bill_number"
    t.integer  "product_id"
    t.text     "product_description"
    t.float    "product_price"
    t.integer  "billdetail_amount"
    t.float    "billdetail_total"
    t.float    "billdetail_net1"
    t.float    "billdetail_net2"
    t.float    "billdetail_net3"
    t.float    "billdetail_net4"
    t.float    "billdetail_discount"
    t.float    "billdetail_tax"
    t.float    "billdetail_tip"
    t.string   "store_id"
    t.string   "pos_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.date     "billdetail_date"
    t.string   "pos_desc"
  end

  create_table "bills", force: :cascade do |t|
    t.integer  "bill_number"
    t.datetime "bill_datetime"
    t.float    "bill_tax"
    t.float    "bill_tip"
    t.float    "bill_total"
    t.float    "bill_discount"
    t.string   "store_id"
    t.string   "pos_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "turn_paymethods", force: :cascade do |t|
    t.string   "paymethod_id"
    t.string   "paymethod_description"
    t.decimal  "system_total"
    t.decimal  "cashbox_total"
    t.decimal  "difference"
    t.string   "turn_id"
    t.string   "store_id"
    t.string   "pos_id"
    t.datetime "creation_time"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "pos_desc"
    t.date     "date"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
